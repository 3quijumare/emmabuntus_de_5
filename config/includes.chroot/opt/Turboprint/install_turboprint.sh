#! /bin/bash

# install_turboprint.sh --
#
#   This file permits to install Turboprint for the Emmabuntüs Equitable Distribs,
#   because there are a bug to install normaly
#
#   Created on 2010-22 by Collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

clear


###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh
emmabuntus=$0


nom_distribution="Emmabuntüs Equitable"

nom_logiciel_affichage=turboprint
nom_paquet=turboprint
nom_logiciel_exe=turboprint


dir_install=/opt/Turboprint
delai_fenetre=20

#  chargement des messages
. /opt/Install_non_free_softwares/emmabuntus_messages.sh


(

echo "# $msg_install $nom_logiciel_affichage, $thank_waiting"
#Désactivation de Gmenu de Cairo-dock pendant l'installation de logiciel pour éviter les fenêtres de notifications
/usr/bin/cairo_dock_gmenu_off.sh

sudo gdebi -n -q $dir_install/${nom_paquet}*.deb

#Activation de Gmenu de Cairo-dock
/usr/bin/cairo_dock_gmenu_on.sh

echo "100" ;
) |
zenity --progress --pulsate\
       --title="$install_title" \
       --text="$install_text" \
       --width=500 \
       --percentage=0 --auto-close --no-cancel

         if [ $? = "1" ]
         then
           sudo dpkg --configure -a
              #Activation de Gmenu de Cairo-dock
           /usr/bin/cairo_dock_gmenu_on.sh

           zenity --error --timeout=$delai_fenetre \
             --text="$install_cancel"
         else
           sudo dpkg --configure -a
           echo "# Install Completed"
         fi


exit 0
