#! /bin/bash

# enable_cairo_dock.sh --
#
#   This file handles the enable/disable mechanism of the cairo-dock
#   in the Emmabuntüs distribution
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was checked and validated on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

var=$1

clear

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh
emmabuntus=$0

nom_distribution="Emmabuntus Debian Edition 5"

nom_logiciel_affichage="Enable/Disable Emmabuntus dock"

if [[ ${USER} == root ]] ; then
dir_user=/root
else
dir_user=/home/${USER}
fi

repertoire_emmabuntus=${dir_user}/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus

cmd_enable_dock=""

langue=`echo $LANG | cut -d_ -f 1 `

#  chargement des variables d'environnement
# (loading environment variables)
. ${env_emmabuntus}

# Sélection des messages a afficher en fonction de la langue
# Définition par défaut des messages en Anglais
# (Selecting the message to display according to the language,
#  defaulting to English)


message_enable_dock="\n\
$(eval_gettext 'Do you want to enable the Emmabuntüs default dock (Cairo-Dock) ?     ')"

message_disable_dock="\n\
$(eval_gettext 'Do you want to disable the Emmabuntüs default dock (Cairo-Dock) ?     ')"



# Test de l'état du vérrouillage du dock
# (test of the dock locking state)
if ps -A | grep "xfce4-session"
then
    if [[ $Dock_XFCE == "1" ]]
    then
        # Dock actif (dock is enabled)
        message_info_dock="${message_disable_dock}"
        cmd_enable_dock=0
    else
        # Dock inactif (dock is disabled)
        message_info_dock="${message_enable_dock}"
        cmd_enable_dock=1
    fi

elif ps -A | grep "lxsession"
then

    if [[ $Dock_LXDE == "1" ]]
    then
        # Dock actif (dock is enabled)
        message_info_dock="${message_disable_dock}"
        cmd_enable_dock=0
    else
        # Dock inactif (dock is disabled)
        message_info_dock="${message_enable_dock}"
        cmd_enable_dock=1
    fi

elif ps -A | grep "openbox"
then

    if [[ $Dock_OpenBox == "1" ]]
    then
        # Dock actif (dock is enabled)
        message_info_dock="${message_disable_dock}"
        cmd_enable_dock=0
    else
        # Dock inactif (dock is disabled)
        message_info_dock="${message_enable_dock}"
        cmd_enable_dock=1
    fi

elif ps -A | grep "lxqt-session"
then

    if [[ $Dock_LXQT == "1" ]]
    then
        # Dock actif (dock is enabled)
        message_info_dock="${message_disable_dock}"
        cmd_enable_dock=0
    else
        # Dock inactif (dock is disabled)
        message_info_dock="${message_enable_dock}"
        cmd_enable_dock=1
    fi

else

    exit 1

fi


export WINDOW_DIALOG_ENABLE_DOCK='<window title="'${nom_logiciel_affichage}'" icon-name="gtk-dialog-question" resizable="false">
<vbox spacing="0">

<text use-markup="true" wrap="false" xalign="0" justify="3">
<input>echo "'${message_info_dock}'" | sed "s%\\\%%g"</input>
</text>

<hbox spacing="10" space-expand="false" space-fill="false">
<button cancel></button>
<button can-default="true" has-default="true" use-stock="true" is-focus="true">
<label>gtk-ok</label>
<action>exit:OK</action>
</button>
</hbox>

</vbox>
</window>'

MENU_DIALOG_ENABLE_DOCK="$(gtkdialog --center --program=WINDOW_DIALOG_ENABLE_DOCK)"

eval ${MENU_DIALOG_ENABLE_DOCK}
echo "MENU_DIALOG_ENABLE_DOCK=${MENU_DIALOG_ENABLE_DOCK}"


if [ ${EXIT} == "OK" ]
then

    if ! [ ${cmd_enable_dock} = "" ]
    then

        if [[ ${var} == "Init" ]]
        then

            /usr/bin/lock_cairo_dock_exec.sh ${cmd_enable_dock} ${env_emmabuntus}

        else

            pkexec /usr/bin/enable_cairo_dock_exec.sh ${cmd_enable_dock} ${env_emmabuntus}

            # Redémarrage du dock pour prendre en compte les modifications de la configuration
            # (Restarting the dock to take into account the modifications of the configuration)

            #  chargement des variables d'environnement
            # (loading environment variables)
            . ${env_emmabuntus}

            # Test de l'état du vérrouillage du dock
            # (test of the dock locking state)
            if ps -A | grep "xfce4-session"
            then
                if [[ $Dock_XFCE == "1" ]]
                then
                    /usr/bin/start_cairo_dock.sh &
                    xfconf-query -v -c xfce4-panel -p /plugins/plugin-9 -s "separator"
                    xfconf-query -v -c xfce4-panel -p /plugins/plugin-10 -s "separator"
                    xfconf-query -v -c xfce4-panel -p /plugins/plugin-11 -s "separator"
                    pkill xfconfd
                    xfce4-panel --restart
                else
                    pkill cairo-dock
                    xfconf-query -v -c xfce4-panel -p /plugins/plugin-9 -s "tasklist"
                    if [[ ! $(xfconf-query -c xfce4-panel -p /plugins/plugin-9/grouping | grep [0-1]) ]] ; then
                        xfconf-query -v -c xfce4-panel -p /plugins/plugin-9/grouping -n -t int -s 1
                    fi
                    xfconf-query -v -c xfce4-panel -p /plugins/plugin-10 -s "showdesktop"
                    xfconf-query -v -c xfce4-panel -p /plugins/plugin-11 -s "pager"
                    pkill xfconfd
                    xfce4-panel --restart
                fi

            elif ps -A | grep "lxsession"
            then

                if [[ $Dock_LXDE == "1" ]]
                then
                    /usr/bin/start_cairo_dock.sh &
                else
                    pkill cairo-dock
                fi

            elif ps -A | grep "openbox"
            then

                if [[ $Dock_OpenBox == "1" ]]
                then
                    /usr/bin/start_cairo_dock.sh &
                else
                    pkill cairo-dock
                fi

            elif ps -A | grep "lxqt-session"
            then

                if [[ $Dock_LXQT == "1" ]]
                then
                    /usr/bin/start_cairo_dock.sh &
                else
                    pkill cairo-dock
                fi
            fi

        fi

    fi

fi






