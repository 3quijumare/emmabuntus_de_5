#! /bin/bash

# emmabuntus_freetube_exe.sh --
#
#   This file permits to install Freetube for the Emmabuntüs distrib.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

clear

dir_home=$1

if [ -f ${dir_home}/freetube_amd64.deb ] ; then

    sudo gdebi -q -n ${dir_home}/freetube_amd64.deb

    sudo rm /usr/share/applications/freetube_install.desktop

fi




