Emmabuntüs
==========

**Emmabuntüs** a été conçue par le [collectif Emmabuntüs](https://collectif.emmabuntus.org/) pour faciliter le reconditionnement des ordinateurs donnés aux associations humanitaires, en particulier les communautés **[Emmaüs](http://fr.wikipedia.org/wiki/Mouvement_Emma%C3%BCs)** (d'où son nom) et favoriser la découverte de [GNU](http://fr.wikipedia.org/wiki/Gnu)/[Linux](http://fr.wikipedia.org/wiki/Linux) par les débutants, mais aussi prolonger la durée de vie du matériel pour limiter le gaspillage entraîné par la surconsommation de matières premières. Cette Millième distribution Linux se veut épurée, accessible, équitable, voir les articles publiés dans [Linux-Essentiel - Août-Septembre 2013](https://emmabuntus.org/linux-essentiel-33-emmabuntus-le-reconditionnement-pour-tous/) et [L’âge de faire - Mars 2014](https://emmabuntus.org/lage-de-faire-mars-2014/).

Emmabuntüs contient tous les logiciels nécessaires à une prise en main rapide et agréable de GNU/Linux.

Il a été nécessaire de trouver un compromis pour faciliter l'accès à tous : ainsi on retrouve des logiciels non libres comme Skype, en attendant l'arrivée d'alternatives permettant aux néophytes une prise en main rapide. Afin de simplifier la navigation, un [Dock](http://fr.wikipedia.org/wiki/Dock_(Informatique)) a été ajouté, il donne accès à tous les logiciels de la suite.

----------

Caractéristiques de ces distributions
----------

- Distributions basées sur des variantes de Debian [8 Jessie](http://fr.wikipedia.org/wiki/Debian#Versions_8.x), [9 Stretch](http://fr.wikipedia.org/wiki/Debian#Versions_9.x), [10 Buster](https://fr.wikipedia.org/wiki/Debian#Version_10.x), [11 Bullseye](https://fr.wikipedia.org/wiki/Debian#Version_11.x) et maintenant [12 Bookworm](https://fr.wikipedia.org/wiki/Debian#Version_12.x)
- Mode d'installation rapide grâce à des scripts d'automatisation (preseed)
- Mode Live sur DVD, Clé USB  ([Ventoy](https://www.ventoy.net/), [Multisystem](http://www.framasoft.net/article5077.html))
- Installation autonome sans Internet, tout est dans la distribution
- Installation ou pas des logiciels non libres comme les fontes Microsoft
- Plus 60 applications supplémentaires
- Un dock permettant l'accès à l'ensemble des applications
- 7 langues (Fr, En, Es, Pt, It, De, Da) prises en charge dans les distributions

----------

Sites
----------

- [Site officiel](https://emmabuntus.org/)
- [Téléchargement](http://download.emmabuntus.org)
- [Tutoriels](http://tutos.emmabuntus.org)
- [Avis techniques sur DistroWatch](http://distrowatch.com/emmabuntus)
- [Wikipédia](http://fr.wikipedia.org/wiki/Emmabunt%C3%BCs)
- [Revues de presse](http://newspapers.emmabuntus.org/)
- [Forum](http://forum.emmabuntus.org/)
- [Blog](http://blog.emmabuntus.org/)
- [Framapiaf](https://framapiaf.org/@Emmabuntus), [Twitter](https://twitter.com/Emmabuntus), [Facebook](http://www.facebook.com/Emmabuntus)

