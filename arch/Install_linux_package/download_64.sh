#!/usr/bin/tclsh

# ~/Travail/EmmabuntusDE4_64/Emmabuntus_DE_4/binary/pool/main/l/linux-signed-amd64


set arch amd64
set version 64
set version_kernel 6.1.0-7-amd64
set version_kernel_detail 6.1.20-2

set options ""
set base_url http://ftp.fr.debian.org/debian/pool/main/l/linux-signed-amd64

set list_paquets [ list acpi-modules ata-modules btrfs-modules cdrom-core-modules crc-modules crypto-dm-modules crypto-modules efi-modules event-modules ext4-modules fat-modules fb-modules firewire-core-modules fuse-modules i2c-modules input-modules isofs-modules jfs-modules linux-image kernel-image loop-modules md-modules mmc-core-modules mouse-modules mtd-core-modules multipath-modules nbd-modules nic-modules nic-pcmcia-modules nic-shared-modules nic-usb-modules nic-wireless-modules pata-modules pcmcia-modules pcmcia-storage-modules ppp-modules sata-modules scsi-core-modules scsi-modules scsi-nic-modules serial-modules sound-modules speakup-modules squashfs-modules udf-modules uinput-modules usb-modules usb-serial-modules usb-storage-modules xfs-modules]

if { [file exists ${version}] == 0 } {
file mkdir ${version}
}

foreach nom_paquet $list_paquets {
   #puts $nom_paquet
   if { $nom_paquet == "linux-image" } {
      set paquet_dl ${nom_paquet}-${version_kernel}_${version_kernel_detail}_${arch}.deb
      if { [file exists ${version}/${paquet_dl}] == 0 } {
        puts "Download $nom_paquet"
        catch {exec wget -O ${version}/${paquet_dl} ${base_url}/${paquet_dl}}

      }
   } else {
      set paquet_dl ${nom_paquet}-${version_kernel}-di_${version_kernel_detail}_${arch}.udeb
      if { [file exists ${version}/${paquet_dl}] == 0 } {
        puts "Download $nom_paquet"
        catch {exec wget -O ${version}/${paquet_dl} ${base_url}/${paquet_dl}}
      }
   }
}


